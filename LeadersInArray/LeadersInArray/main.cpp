#include<iostream>
#include<vector>
#include <climits>

using namespace std;

class Engine
{
public:
	   void printLeadersInArray(vector<int> arrVector)
	   {
           int len = (int)arrVector.size() - 1;
           int currLeader = INT_MIN;
           while(len >= 0)
           {
               if(arrVector[len] > currLeader)
               {
                   cout<<arrVector[len]<<" ";
                   currLeader = arrVector[len];
               }
               len--;
           }
           cout<<endl;
       }
};

int main()
{
    int arr[] = {98, 23, 54, 12, 20, 7, 27};
    vector<int> arrVector;
    int len = sizeof(arr)/sizeof(arr[0]);
    for(int i = 0 ; i < len ; i++)
    {
        arrVector.push_back(arr[i]);
    }
    Engine e = Engine();
    e.printLeadersInArray(arrVector);
    return 0;
}
